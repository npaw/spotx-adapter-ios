//
//  Constants.m
//  iOSObjc
//
//  Created by nice on 30/10/2019.
//  Copyright © 2019 nice. All rights reserved.
//

#import <Foundation/Foundation.h>

NSString *const SPOT_X_TEST_CHANNEL = @"85394";
NSString *const BASIC_SEGUE_IDENTIFIER = @"basicSegue";
NSString *const UNKNOWN_SEGUE_IDENTIFIER = @"unknown";
NSString *const VIDEO_URL = @"https://cdn.spotxcdn.com/media/videos/orig/d/3/d35ba3e292f811e5b08c1680da020d5a.mp4";
