//
//  Constants.h
//  iOSObjc
//
//  Created by nice on 30/10/2019.
//  Copyright © 2019 nice. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

extern NSString *const SPOT_X_TEST_CHANNEL;
extern NSString *const BASIC_SEGUE_IDENTIFIER;
extern NSString *const UNKNOWN_SEGUE_IDENTIFIER;
extern NSString *const VIDEO_URL;

#endif /* Constants_h */
